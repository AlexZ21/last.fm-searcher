cmake_minimum_required(VERSION 3.1)

project(network)

set (CMAKE_CXX_STANDARD 14)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/cmake/Modules/")
include(targetos)

if (SYSTEM_WIN)
    include(opensslwin)
endif (SYSTEM_WIN)

file(GLOB NETWORK_SRC
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.cpp
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.c
    ${CMAKE_CURRENT_SOURCE_DIR}/src/*.h
    )

# Unix socket implementation
if (SYSTEM_LINUX)
    file(GLOB NETWORK_UNIX_SRC
        ${CMAKE_CURRENT_SOURCE_DIR}/src/unix/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/unix/*.c
        ${CMAKE_CURRENT_SOURCE_DIR}/src/unix/*.h
        )
    set(NETWORK_SRC ${NETWORK_SRC} ${NETWORK_UNIX_SRC})
endif(SYSTEM_LINUX)

# Windows socket implementation
if (SYSTEM_WIN)
    file(GLOB NETWORK_WIN_SRC
        ${CMAKE_CURRENT_SOURCE_DIR}/src/win/*.cpp
        ${CMAKE_CURRENT_SOURCE_DIR}/src/win/*.c
        ${CMAKE_CURRENT_SOURCE_DIR}/src/win/*.h
        )
    set(NETWORK_SRC ${NETWORK_SRC} ${NETWORK_WIN_SRC})
endif(SYSTEM_WIN)

add_definitions(-DNETWORK_LIBRARY)
add_library(${PROJECT_NAME} SHARED ${NETWORK_SRC})

#add_library(${PROJECT_NAME} STATIC ${NETWORK_SRC})

# log
add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/3rd/log)
set(NETWORK_LOG network_log)
set(NETWORK_LOG_INCLUDE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/3rd/log/src)
include_directories(${NETWORK_LOG_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} ${NETWORK_LOG})

if (SYSTEM_WIN)
    include_directories(${OPENSSL_INCLUDE_DIR})
    target_link_libraries(${PROJECT_NAME} ${OPENSSL_LIBS} ws2_32)
    set(THIRD_PARTY_LIBS ${OPENSSL_LIBS})
endif (SYSTEM_WIN)

if (SYSTEM_LINUX)
    target_link_libraries(${PROJECT_NAME} ssl crypto)
endif (SYSTEM_LINUX)

