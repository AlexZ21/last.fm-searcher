if (UNIX)
    set(SYSTEM_LINUX 1)
endif (UNIX)

if (WIN32)
    set(SYSTEM_WIN 1)
endif (WIN32)

IF (SYSTEM_LINUX)
	add_definitions(-DSYSTEM_LINUX)
    MESSAGE (STATUS "Operating system is Linux")
ELSEIF (SYSTEM_WIN)
	add_definitions(-DSYSTEM_WIN)
    MESSAGE (STATUS "Operating system is Windows")
ELSE (SYSTEM_LINUX)
ENDIF (SYSTEM_LINUX)
