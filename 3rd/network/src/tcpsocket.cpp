#include "tcpsocket.h"
#include "ipaddress.h"
#include "socketimpl.h"
#include "socketselector.h"
#include <log.h>

#include <algorithm>
#include <cstring>

#ifdef _MSC_VER
#pragma warning(disable: 4127)
#endif


namespace {
#ifdef SYSTEM_LINUX
const int flags = MSG_NOSIGNAL;
#else
const int flags = 0;
#endif
}

namespace network {

TcpSocket::TcpSocket() :
    Socket(Tcp)
{

}

unsigned short TcpSocket::localPort() const
{
    if (handle() != SocketImpl::invalidSocket()) {
        sockaddr_in address;
        SocketImpl::AddrLength size = sizeof(address);
        if (getsockname(handle(), reinterpret_cast<sockaddr*>(&address), &size) != -1)
            return ntohs(address.sin_port);
    }

    return 0;
}

IpAddress TcpSocket::remoteAddress() const
{
    if (handle() != SocketImpl::invalidSocket()) {
        sockaddr_in address;
        SocketImpl::AddrLength size = sizeof(address);
        if (getpeername(handle(), reinterpret_cast<sockaddr*>(&address), &size) != -1)
            return IpAddress(ntohl(address.sin_addr.s_addr));
    }

    return IpAddress::None;
}

unsigned short TcpSocket::remotePort() const
{
    if (handle() != SocketImpl::invalidSocket()) {
        sockaddr_in address;
        SocketImpl::AddrLength size = sizeof(address);
        if (getpeername(handle(), reinterpret_cast<sockaddr*>(&address), &size) != -1)
            return ntohs(address.sin_port);
    }

    return 0;
}

Socket::Status TcpSocket::connect(const IpAddress& remAddr, unsigned short remPort,
                                  std::chrono::milliseconds timeout)
{
    create();

    sockaddr_in address = SocketImpl::createAddress(remAddr.toInteger(), remPort);

    if (timeout.count() <= 0) {
        if (::connect(handle(), reinterpret_cast<sockaddr*>(&address), sizeof(address)) == -1)
            return SocketImpl::getErrorStatus();
        return Done;

    } else {
        bool blocking = isBlocking();

        if (blocking)
            setBlocking(false);

        if (::connect(handle(), reinterpret_cast<sockaddr*>(&address), sizeof(address)) >= 0) {
            setBlocking(blocking);
            return Done;
        }

        Status status = SocketImpl::getErrorStatus();

        if (!blocking)
            return status;

        if (status == Socket::NotReady) {
            fd_set selector;
            FD_ZERO(&selector);
            FD_SET(handle(), &selector);

            timeval time;
            time.tv_sec  = static_cast<long>(std::chrono::duration_cast<std::chrono::microseconds>
                                             (timeout).count() / 1000000);
            time.tv_usec = static_cast<long>(std::chrono::duration_cast<std::chrono::microseconds>
                                             (timeout).count() % 1000000);

            if (select(static_cast<int>(handle() + 1), NULL, &selector, NULL, &time) > 0) {
                if (remoteAddress() != IpAddress::None) {
                    status = Done;
                } else {
                    status = SocketImpl::getErrorStatus();
                }
            } else {
                status = SocketImpl::getErrorStatus();
            }
        }

        setBlocking(true);

        return status;
    }
}

void TcpSocket::disconnect()
{
    close();
}

Socket::Status TcpSocket::send(const void* data, std::size_t size)
{
    if (!isBlocking())
        LWARN("Partial sends might not be handled properly.");

    std::size_t sent;

    return send(data, size, sent);
}

Socket::Status TcpSocket::send(const void* data, std::size_t size, std::size_t& sent)
{
    if (!data || (size == 0)) {
        LERR("Cannot send data over the network (no data to send)");
        return Error;
    }

    int result = 0;
    for (sent = 0; sent < size; sent += result)
    {
        result = ::send(handle(), static_cast<const char*>(data) + sent, size - sent, flags);
        if (result < 0) {
            Status status = SocketImpl::getErrorStatus();

            if ((status == NotReady) && sent)
                return Partial;

            return status;
        }
    }

    return Done;
}

Socket::Status TcpSocket::receive(void* data, std::size_t size, std::size_t& received)
{
    received = 0;

    if (!data) {
        LERR("Cannot receive data from the network (the destination buffer is invalid)");
        return Error;
    }

    int sizeReceived = recv(handle(), static_cast<char*>(data), static_cast<int>(size), flags);

    if (sizeReceived > 0) {
        received = static_cast<std::size_t>(sizeReceived);
        return Done;

    } else if (sizeReceived == 0) {
        return Socket::Disconnected;

    } else {
        return SocketImpl::getErrorStatus();
    }
}

Socket::Status TcpSocket::receive(void *data, std::size_t size, std::size_t &received,
                                  std::chrono::milliseconds timeout)
{
    if (timeout.count() == 0)
        return receive(data, size, received);

    SocketSelector selector;
    selector.add(*this);
    if (selector.wait(timeout)) {
        return receive(data, size, received);
    } else {
        return Socket::Timeout;
    }
}

}
