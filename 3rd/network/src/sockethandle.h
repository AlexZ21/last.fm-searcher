#ifndef SOCKETHANDLE_H
#define SOCKETHANDLE_H

#include "network_global.h"

#if defined(SYSTEM_WIN)
    #include <basetsd.h>
#endif

namespace network {

#if defined(SYSTEM_WIN)
    typedef UINT_PTR SocketHandle;
#else
    typedef int SocketHandle;
#endif

}

#endif // SOCKETHANDLE_H
