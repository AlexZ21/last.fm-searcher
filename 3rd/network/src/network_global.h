#ifndef NETWORK_GLOBAL_H
#define NETWORK_GLOBAL_H

#if defined(_MSC_VER)
#  define DECL_EXPORT     __declspec(dllexport)
#  define DECL_IMPORT     __declspec(dllimport)
#elif defined(__GNUC__)
#  if  defined(__WIN32__)
#    define DECL_EXPORT     __declspec(dllexport)
#    define DECL_IMPORT     __declspec(dllimport)
#  elif defined(__linux__)
#    define DECL_EXPORT     __attribute__((visibility("default")))
#    define DECL_IMPORT     __attribute__((visibility("default")))
#    define DECL_HIDDEN     __attribute__((visibility("hidden")))
#  endif
#endif

#define MACORE_EXTERN extern "C"

#if defined(NETWORK_LIBRARY)
#  define NETWORK_EXPORT DECL_EXPORT
#else
#  define NETWORK_EXPORT DECL_IMPORT
#endif

#endif // NETWORK_GLOBAL_H
