#ifndef SOCKET_H
#define SOCKET_H

#include "network_global.h"
#include "sockethandle.h"

#include <vector>

namespace network {

class SocketSelector;

class NETWORK_EXPORT Socket
{
public:
    enum Status {
        Done,
        NotReady,
        Timeout,
        Partial,
        Disconnected,
        Error
    };

    enum {
        AnyPort = 0
    };

    virtual ~Socket();

    void setBlocking(bool blocking);
    bool isBlocking() const;

    SocketHandle handle() const;

protected:
    enum Type {
        Tcp,
        Udp
    };

    Socket(Type type);

    void create();
    void create(SocketHandle handle);
    void close();

private:
    friend class SocketSelector;

    Type m_type;
    SocketHandle m_socket;
    bool m_isBlocking;

};

}

#endif
