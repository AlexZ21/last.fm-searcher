#ifndef SOCKETSELECTOR_HPP
#define SOCKETSELECTOR_HPP

#include "network_global.h"

#include <chrono>

namespace network {

class Socket;

class NETWORK_EXPORT SocketSelector
{
public:
    SocketSelector();
    SocketSelector(const SocketSelector& copy);
    ~SocketSelector();

    void add(Socket& socket);
    void remove(Socket& socket);

    void clear();

    bool wait(const std::chrono::milliseconds &timeout = std::chrono::milliseconds(0));

    bool isReady(Socket& socket) const;

    SocketSelector& operator =(const SocketSelector& right);

private:
    struct SocketSelectorImpl;

    SocketSelectorImpl *m_impl;
};

}

#endif // SOCKETSELECTOR_HPP
