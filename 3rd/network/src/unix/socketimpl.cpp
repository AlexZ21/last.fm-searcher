#include "socketimpl.h"
#include <log.h>

#include <errno.h>
#include <fcntl.h>
#include <cstring>

namespace network {

sockaddr_in SocketImpl::createAddress(uint32_t address, unsigned short port)
{
    sockaddr_in addr;
    std::memset(&addr, 0, sizeof(addr));
    addr.sin_addr.s_addr = htonl(address);
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);

    return addr;
}

SocketHandle SocketImpl::invalidSocket()
{
    return -1;
}

void SocketImpl::close(SocketHandle sock)
{
    ::close(sock);
}

void SocketImpl::setBlocking(SocketHandle sock, bool block)
{
    int status = fcntl(sock, F_GETFL);
    if (block) {
        if (fcntl(sock, F_SETFL, status & ~O_NONBLOCK) == -1)
            LERR("Failed to set file status flags: ", errno);
    } else {
        if (fcntl(sock, F_SETFL, status | O_NONBLOCK) == -1)
            LERR("Failed to set file status flags: ", errno);

    }
}

Socket::Status SocketImpl::getErrorStatus()
{
    if ((errno == EAGAIN) || (errno == EINPROGRESS))
        return Socket::NotReady;

    switch (errno) {
        case EWOULDBLOCK:  return Socket::NotReady;
        case ECONNABORTED: return Socket::Disconnected;
        case ECONNRESET:   return Socket::Disconnected;
        case ETIMEDOUT:    return Socket::Disconnected;
        case ENETRESET:    return Socket::Disconnected;
        case ENOTCONN:     return Socket::Disconnected;
        case EPIPE:        return Socket::Disconnected;
        default:           return Socket::Error;
    }
}

}
