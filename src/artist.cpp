#include "artist.h"

Artist::Artist(const std::string &name, const std::string &mbid) :
    m_name(name),
    m_mbid(mbid)
{

}

std::string Artist::name() const
{
    return m_name;
}

std::string Artist::mbid() const
{
    return m_mbid;
}
