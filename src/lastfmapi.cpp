#include "lastfmapi.h"
#include "md5.h"

#include <log.h>
#include <http.h>
#include <json.hpp>

#include <string>
#include <algorithm>
#include <fstream>

#ifdef __WIN32__
#   include <windows.h>
#endif

LastfmApi::LastfmApi() :
    m_useSession(false),
    m_base("http://ws.audioscrobbler.com/2.0/")
{

}

bool LastfmApi::restoreSession()
{
    std::ifstream ifs("session");
    if (!ifs.is_open())
        return false;
    ifs >> instance().m_apiKey;
    ifs >> instance().m_sharedSecret;
    ifs >> instance().m_sessionKey;
    ifs >> instance().m_name;

    std::string response = method("user.getInfo", {{"user", instance().m_name}});
    nlohmann::json json;
    try {
        json = nlohmann::json::parse(response);
        if (!json["error"].is_null()) {
            LERR(json["message"].get<std::string>());
            return false;

        }
    } catch (const std::exception &err) {
        LERR("Error parse json response: ", err.what());
        return false;
    }

    instance().m_useSession = true;

    return true;
}

bool LastfmApi::auth(const std::string &apiKey, const std::string &sharedKey)
{
    instance().m_apiKey = apiKey;
    instance().m_sharedSecret = sharedKey;

    std::string tokenJson = call("method=auth.gettoken&format=json&api_key=" + instance().m_apiKey);
    if (tokenJson.empty())
        return false;

    nlohmann::json json;
    try {
        json = nlohmann::json::parse(tokenJson);
        if (!json["error"].is_null()) {
            LERR(json["message"].get<std::string>());
            return false;

        }
        instance().m_token = json["token"].get<std::string>();
        if (instance().m_token.empty()) {
            LERR("Token not found in response");
            return false;
        }
    } catch (const std::exception &err) {
        LERR("Error parse json response: ", err.what());
        return false;
    }

    return true;
}

bool LastfmApi::startSession()
{
    std::string appSigSrc = concatenateParameters({{"api_key", instance().m_apiKey},
                                                   {"token", instance().m_token},
                                                   {"method", "auth.getSession"}
                                                  });
    appSigSrc.append(instance().m_sharedSecret);
    std::string appSig = md5(appSigSrc);

    std::string response = call("method=auth.getSession&api_key=" + instance().m_apiKey + "&token=" +
                                instance().m_token + "&api_sig=" + appSig + "&format=json");

//    LDEBUG(response);

    nlohmann::json json;
    try {
        json = nlohmann::json::parse(response);
        if (!json["error"].is_null()) {
            LERR(json["message"].get<std::string>());
            return false;

        }
        nlohmann::json sessionObject = json["session"].get<nlohmann::json>();
        instance().m_sessionKey = sessionObject["key"].get<std::string>();
        instance().m_name = sessionObject["name"].get<std::string>();
        if (instance().m_sessionKey.empty()) {
            LERR("Session key not found in response");
            return false;
        }
    } catch (const std::exception &err) {
        LERR("Error parse json response: ", err.what());
        return false;
    }

    std::ofstream ofs("session");
    ofs << instance().m_apiKey << std::endl;
    ofs << instance().m_sharedSecret << std::endl;
    ofs << instance().m_sessionKey << std::endl;
    ofs << instance().m_name << std::endl;
    ofs.flush();

    instance().m_useSession = true;

    return true;
}

std::string LastfmApi::method(const std::string &method, const LastfmApi::parameters &params)
{
    std::string response;

    std::string paramsStr;
    for (std::pair<std::string, std::string> v : params)
        paramsStr.append("&").append(v.first).append("=").append(v.second);

    if (instance().m_useSession) {
        LDEBUG("Call with session key: ", method);
        LastfmApi::parameters p = params;
        p.emplace_back("api_key", instance().m_apiKey);
        p.emplace_back("sk", instance().m_sessionKey);
        p.emplace_back("method", method);
        std::string appSigSrc = concatenateParameters(p);
        appSigSrc.append(instance().m_sharedSecret);
        std::string appSig = md5(appSigSrc);

        response = call("method=" + method +
                        paramsStr +
                        "&sk=" + instance().m_sessionKey +
                        "&api_key=" + instance().m_apiKey +
                        "&api_sig=" + appSig +
                        "&format=json");

    } else {
        LDEBUG("Call: ", method);
        response = call("method=" + method +
                        paramsStr +
                        "&api_key=" + instance().m_apiKey +
                        "&format=json");
    }


    return response;
}

std::string LastfmApi::call(const std::string &method)
{
    std::chrono::time_point<std::chrono::steady_clock> start, end;
    start = std::chrono::steady_clock::now();

    network::Http http;
    http.setFollowLocation(true);
    http.setStoreBody(true);
    http.setConnectionTimeout(std::chrono::milliseconds(1000));

    LDEBUG("Send request: ", instance().m_base + "?" + method);
    network::Http::Request req(network::Url(instance().m_base + "?" + method));
    network::Http::Response res = http.sendRequest(req, std::chrono::milliseconds(5000));

    end = std::chrono::steady_clock::now();

    uint32_t elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count();
    LDEBUG("Elapsed msec: ", elapsed);

    return res.body();
}

void LastfmApi::requestAuthorization()
{
    std::string url = "\"http://www.last.fm/api/auth/?api_key=" + instance().m_apiKey + "&token=" + instance().m_token + "\"";
#ifdef __WIN32__
    ShellExecuteA(0, 0, url.c_str(), 0, 0 , SW_SHOW );
#else
    std::string cmd = "xdg-open " + url;
    system(cmd.c_str());
#endif
}

std::string LastfmApi::concatenateParameters(const LastfmApi::parameters &params)
{
    parameters p = params;
    std::sort(p.begin(), p.end());
    std::string out;
    for (std::pair<std::string, std::string> v : p)
        out.append(v.first).append(v.second);
    return out;
}
