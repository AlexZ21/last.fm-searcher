#include "lastfmapi.h"
#include "artist.h"

#include <log.h>
#include <json.hpp>

#include <thread>
#include <iostream>
#include <vector>

int main(int argc, char *argv[])
{
    LENABLE;

    if (!LastfmApi::restoreSession()) {
        if (!LastfmApi::auth("ffb98eae19542992ca3fad4e2c22e98f", "80c16c71117c696ca24e1c39cbb978cf"))
            exit(EXIT_FAILURE);
        LastfmApi::requestAuthorization();
        LINFO("Open browser for authorization");
        int secs = 10;
        while (secs > 0) {
            LINFO("Timeout: ", secs);
            std::this_thread::sleep_for(std::chrono::seconds(1));
            --secs;
        }
        if (!LastfmApi::startSession())
            exit(EXIT_FAILURE);
    }

    std::vector<Artist> m_artists;

    // Loading artists
    uint32_t currentPage = 1;
    uint32_t perPage = 100;
    uint32_t totalPages = 2;

    while (currentPage <= totalPages) {
        LSDEBUG("Process page:", currentPage, totalPages);
        std::string response = LastfmApi::method("tag.gettopartists",
        {{"tag", "Witch house"},
         {"limit", std::to_string(perPage)},
         {"page", std::to_string(currentPage)}});

        //        LDEBUG(response);
        //        return 0;

        LDEBUG("Parsing page");
        nlohmann::json json;
        try {
            json = nlohmann::json::parse(response);
            if (!json["error"].is_null()) {
                LERR(json["message"].get<std::string>());
                return false;

            }
            nlohmann::json attrsObject = json["topartists"].get<nlohmann::json>()["@attr"].get<nlohmann::json>();
            totalPages = std::atoi(attrsObject["totalPages"].get<std::string>().c_str());

            nlohmann::json artistsArray = json["topartists"].get<nlohmann::json>()["artist"].get<nlohmann::json>();

            for (auto &artistObject : artistsArray)
                m_artists.emplace_back(artistObject["name"].get<std::string>(), "1"/*artistObject["mbid"].get<std::string>()*/);

        } catch (const std::exception &err) {
            LERR("Error parse json response: ", err.what());
            return false;
        }

        ++currentPage;
    }

    for (const Artist &artist : m_artists) {
        std::cout << artist.name() << std::endl;
    }

    return 0;
}
