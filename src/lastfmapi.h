#ifndef LASTFMAPI_H
#define LASTFMAPI_H

#include <string>
#include <vector>

class LastfmApi
{
public:
    using parameters = std::vector<std::pair<std::string, std::string>>;
    LastfmApi();
    static LastfmApi &instance() { static LastfmApi i; return i; }

    static bool restoreSession();
    static bool auth(const std::string &apiKey, const std::string &sharedKey);
    static void requestAuthorization();
    static bool startSession();
    static std::string method(const std::string &method,
                              const parameters &params = parameters());

private:
    static std::string call(const std::string &method);
    static std::string concatenateParameters(const parameters &params);

private:
    bool m_useSession;
    std::string m_apiKey;
    std::string m_sharedSecret;
    std::string m_token;
    std::string m_sessionKey;
    std::string m_name;
    std::string m_base;

};

#endif // LASTFMAPI_H
