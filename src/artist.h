#ifndef ARTIST_H
#define ARTIST_H

#include <string>

class Artist
{
public:
    Artist(const std::string &name, const std::string &mbid);
    ~Artist() = default;

    std::string name() const;
    std::string mbid() const;

private:
    std::string m_name;
    std::string m_mbid;

};

#endif // ARTIST_H
